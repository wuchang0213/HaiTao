package com.admin.haitao.table

import java.io.Serializable

/**
 * Created by 吴昶 on 2018/7/26.
 */
class InfoDetail:Serializable{
    var id:Int =0
    var title:String=""
    var pubtime:String=""
    var image:String=""
    var imgw:Number = 0
    var imgh:Number = 194
    var iftobuy:Number = 1
    var fromsite:String=""
    var country:String=""
    var mall:String=""
    var buyurl:String=""
    var dealfeature:String=""

    override fun toString(): String {
        return "InfoDetail(id=$id, title='$title', pubtime='$pubtime', image='$image', imgw=$imgw, imgh=$imgh, iftobuy=$iftobuy, fromsite='$fromsite', country='$country', mall='$mall', buyurl='$buyurl', dealfeature='$dealfeature')"
    }


}