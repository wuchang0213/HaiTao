package com.admin.haitao.until

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by 吴昶 on 2018/7/26.
 */
object SharedPreferencesUtil{

    private var sharedPreferences:SharedPreferences ?=null

    fun getInitial(context: Context){
        if(sharedPreferences==null)
            sharedPreferences=context.getSharedPreferences("htgd", Activity.MODE_PRIVATE);
    }

    /**
     * SharedPreferences 存储int值
     * @param key
     * @param value
     */
    fun setSharedPreferencesInteger(key: String, value: Int) {
        val editor = sharedPreferences!!.edit()
        editor.putInt(key, value)
        editor.commit()
    }

    fun getSharedPreferencesInteger(key: String, value: Int): Int {
        return sharedPreferences!!.getInt(key, value)
    }

    /**
     * SharedPreferences 存储字符串
     * @param key
     * @param value
     */
    fun setSharedPreferencesString(key: String, value: String) {
        val editor = sharedPreferences!!.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getSharedPreferencesString(key: String, value: String): String? {
        return sharedPreferences!!.getString(key, value)
    }

    /**
     * SharedPreferences 存储boolean值
     * @param key
     * @param value
     */
    fun setSharedPreferencesBoolean(key: String, value: Boolean) {
        val editor = sharedPreferences!!.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    /**
     *
     * @param key
     * @param value
     * @return
     */
    fun getSharedPreferencesBoolean(key: String, value: Boolean): Boolean {
        return sharedPreferences!!.getBoolean(key, value)
    }
}