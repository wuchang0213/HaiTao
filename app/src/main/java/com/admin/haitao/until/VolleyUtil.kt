package com.admin.haitao.until

import android.content.Context

import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

/**
 * Volley使用
 */
object VolleyUtil {
    private var mRequestQueue: RequestQueue? = null

    /**
     * 回去volley队列
     * @return
     */
    public val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null)
                throw RuntimeException("请先初始化mRequestQueue")
            return mRequestQueue as RequestQueue
        }

    fun initialize(context: Context) {
        if (mRequestQueue == null) {
            synchronized(VolleyUtil::class.java) {
                if (mRequestQueue == null) {
                    mRequestQueue = Volley.newRequestQueue(context)
                }
            }
        }

        mRequestQueue!!.start()
    }

}
