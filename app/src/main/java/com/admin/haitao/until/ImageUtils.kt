package com.admin.haitao.until

import android.content.Context
import android.util.Log
import android.widget.ImageView

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by 吴昶 on 2018/4/11.
 */

object ImageUtils {

    fun GlideLoadImage(context: Context?, url: String, imageView: ImageView?, error: Int, placeholder: Int) {
        Glide.with(context)
                .setDefaultRequestOptions(RequestOptions().error(error).placeholder(placeholder))
                .load(url)
                .into(imageView)
    }
}
