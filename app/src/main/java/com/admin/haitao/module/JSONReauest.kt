package com.admin.haitao.module

import android.content.Context
import android.util.Log
import android.widget.Toast

import com.admin.haitao.until.VolleyUtil
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.JsonRequest
import com.android.volley.toolbox.StringRequest

import org.json.JSONException
import org.json.JSONObject

import java.util.HashMap

/**
 * Created by wuchang on 2016-12-8.
 */
class JSONReauest {

    interface ResultListener {
        fun success(result: JSONObject)
        fun error(volleyError: VolleyError?)
        fun filed(msg: String, errorcode: Int)
    }


    constructor(context: Context, url: String, timeout: Int, map: Map<String, String>, listener: ResultListener) {
        val request = object : StringRequest(Request.Method.POST, url,
                Response.Listener { s ->
                    try {
                        Log.d("", s)
                        val job = JSONObject(s)
                        val success = job.getString("respCode")
                        if (success == "0") {
                            listener.success(job)
                        } else {
                            //                                ToastUtils.showToastByString(context,job.optString("respMsg"));
                            listener.filed(job.optString("respMsg"), 0)
                        }
                    } catch (e: JSONException) {
                        listener.filed(e.message!!, -1)
                    }
                }, Response.ErrorListener { volleyError -> listener.error(volleyError) }) {
            override fun getParams(): Map<String, String> {
                return map
            }

            //重新此方法，设置请求头文件数据
        }
        request.retryPolicy = DefaultRetryPolicy(timeout, 0, 1.0f)//自定义超时时间,最大请求次数，退避乘数

        VolleyUtil.requestQueue.add(request)

    }


    constructor(context: Context, url: String, timeout: Int, listener: ResultListener) {
        val request = StringRequest(Request.Method.GET, url,
                Response.Listener { s ->
                    try {
                        val job = JSONObject(s)
                        val success = job.getString("status")
                        if (success == "ok") {
                            listener.success(job)
                        } else {
                            listener.filed(job.optString("respMsg"), 0)
                        }
                    } catch (e: JSONException) {
                        listener.filed(e.message!!, -1)
                    }
                }, Response.ErrorListener { volleyError ->
            if (volleyError?.message != null) {
                listener.error(volleyError)
            } else {
                Toast.makeText(context, "数据获取失败", Toast.LENGTH_SHORT).show()
            }
        })
        request.retryPolicy = DefaultRetryPolicy(timeout, 0, 1.0f)//自定义超时时间
        VolleyUtil.requestQueue.add(request)

    }

    constructor(context: Context, url: String, timeout: Int, data: JSONObject, listener: ResultListener) {
        val request = JsonObjectRequest(Request.Method.POST, url, data,
                Response.Listener { response ->
                    try {
                        val success = response.getString("respCode")
                        if (success == "0") {
                            listener.success(response)
                        } else {
                            listener.filed(success, 0)
                        }
                    } catch (e: JSONException) {
                        listener.filed(e.message!!, -1)
                    }
                }, Response.ErrorListener { error ->
            if (error?.message != null) {
                listener.error(error)
            } else {
                Toast.makeText(context, "数据获取失败", Toast.LENGTH_SHORT).show()
            }
        })

        request.retryPolicy = DefaultRetryPolicy(timeout, 0, 1.0f)//自定义超时时间
        VolleyUtil.requestQueue.add(request)
    }


}
