package com.admin.haitao.module

import android.content.Context
import android.os.Handler
import android.util.Log
import com.admin.haitao.module.JSONReauest.ResultListener
import com.admin.haitao.table.Result
import com.admin.kotlintest.utils.GsonUtils
import com.android.volley.VolleyError
import org.json.JSONObject

/**
 * Created by 吴昶 on 2018/7/26.
 */
class GetServiceData(){

companion object {
    fun getData(context: Context,url:String,data:Map<String,String>,handler: Handler,requestCode:Int){
        JSONReauest(
                context,
                url,
                10000,
                data,
                object :ResultListener{
                    override fun success(result: JSONObject) {
                        val dataResult=GsonUtils.paresJsonWithGon(result.toString(),Result::class.java)
                        sendHandler(handler,dataResult,requestCode)
                    }

                    override fun error(volleyError: VolleyError?) {

                    }

                    override fun filed(msg: String, errorcode: Int) {

                    }

                }

        )
    }

    fun <T> getData(context: Context,url:String,handler: Handler,requestCode:Int,t :Class<T>){
        JSONReauest(
                context,
                url,
                10000,
                object :ResultListener{
                    override fun success(result: JSONObject) {
                        Log.d("===",url)
                        val dataResult=GsonUtils.paresJsonWithGon(result.toString(),t)
                        sendHandler(handler, dataResult!!,requestCode)
                    }

                    override fun error(volleyError: VolleyError?) {

                    }

                    override fun filed(msg: String, errorcode: Int) {

                    }

                }

        )
    }

    fun sendHandler(handler: Handler,obj:Any,what:Int){
        val message=handler.obtainMessage()
        message.obj=obj
        message.what=what
        handler.sendMessage(message)
    }
}




}