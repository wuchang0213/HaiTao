package com.admin.haitao.fragment

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by 吴昶 on 2018/8/1.
 */
abstract class BaseFragment:Fragment(){

    var fview:View?=null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        if(fview==null) {  //避免重复初始化fragment
            fview = inflater!!.inflate(bindLayout(), null)
            initData()
        }
        val parent=fview!!.parent
        if(parent!=null){
            (parent as ViewGroup).removeView(fview)
        }
        return fview!!
    }

    /**
     * 布局文件绑定
     */
    abstract fun bindLayout():Int

    /**
     * 数据初始化
     */
    abstract fun initData()
}