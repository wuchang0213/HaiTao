package com.admin.haitao.fragment

import com.admin.haitao.R
import com.admin.haitao.module.GetServiceData
import com.admin.haitao.table.Result
import com.admin.haitao.until.SharedPreferencesUtil


/**
 * Created by 吴昶 on 2018/7/26.
 * 首页
 */
class HomepagerFragment:HtAndHomeFragment() {

    override fun assetsTypeData(): String {
        return "main_type_data.json"
    }

    override fun recordFId(cnf: Int) {
        SharedPreferencesUtil.setSharedPreferencesInteger("cnf", cnf)
    }

    override fun recordEId(cne: Int) {
        SharedPreferencesUtil.setSharedPreferencesInteger("cne", cne)
    }

    override fun themeColor(): Int {
        return R.color.index
    }

    override fun getData(){
        val markId=SharedPreferencesUtil.getSharedPreferencesInteger("cnf",5685521)
        GetServiceData.getData(activity, "$url?markid=$markId$urlConnection",handler,0,Result::class.java)
    }

    override fun loadMore(){
        val markId=SharedPreferencesUtil.getSharedPreferencesInteger("cne",5685521)
        GetServiceData.getData(activity, "$url?sinceid=$markId$urlConnection",handler,1,Result::class.java)
    }

}