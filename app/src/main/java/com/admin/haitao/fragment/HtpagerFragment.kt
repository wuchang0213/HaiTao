package com.admin.haitao.fragment

import com.admin.haitao.R
import com.admin.haitao.module.GetServiceData
import com.admin.haitao.table.Result
import com.admin.haitao.until.SharedPreferencesUtil

/**
 * Created by 吴昶 on 2018/7/26.
 * 海淘
 */
class HtpagerFragment:HtAndHomeFragment(){

    override fun assetsTypeData(): String {
        return "ht_type_data.json"
    }

    override fun recordFId(cnf: Int) {
        SharedPreferencesUtil.setSharedPreferencesInteger("usf", cnf)
    }

    override fun recordEId(cne: Int) {
        SharedPreferencesUtil.setSharedPreferencesInteger("use", cne)
    }

    override fun themeColor(): Int {
        return R.color.ht_title
    }

    override fun getData(){
        val markId=SharedPreferencesUtil.getSharedPreferencesInteger("usf",5685521)
        GetServiceData.getData(activity, "$url?markid=$markId$urlConnection&country=us",handler,0,Result::class.java)
    }

    override fun loadMore(){
        val markId=SharedPreferencesUtil.getSharedPreferencesInteger("use",5685521)
        GetServiceData.getData(activity, "$url?sinceid=$markId$urlConnection&country=us",handler,1,Result::class.java)
    }
}