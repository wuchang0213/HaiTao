package com.admin.haitao.fragment

import android.annotation.SuppressLint
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.view.View
import com.admin.haitao.R
import com.admin.haitao.activity.InfoDetailActivity
import com.admin.haitao.adapter.TrankAdapter
import com.admin.haitao.module.GetServiceData
import com.admin.haitao.table.InfoDetail
import com.admin.haitao.table.Result
import kotlinx.android.synthetic.main.fragment_trank.view.*
import kotlinx.android.synthetic.main.layout_title.view.*
import org.jetbrains.anko.startActivity
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by 吴昶 on 2018/7/26.
 */
class HtrankFragment:BaseFragment(){
    private var data=ArrayList<InfoDetail>()
    private var adapter:TrankAdapter?=null
    private var url="https://guangdiu.com/api/getranklist.php"
    private var day=""
    private var time=0
    private var hour=0

    private var handler=Handler(Handler.Callback {
        when(it.what){
            0->{
                val result=it!!.obj as Result
                    data.clear()
                    data.addAll(result.data!!)
                    adapter!!.upData(data)
                    fview!!.srl_trank_fresh.finishRefresh()
            }
        }
        false
    })

    override fun bindLayout(): Int {
        return R.layout.fragment_trank
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun initData(){
        fview!!.line_bar.setBackgroundColor(resources.getColor(R.color.trank_title,null))
        fview!!.iv_title_src.visibility=View.GONE
        fview!!.tv_h_title.text="小时风云榜"
        adapter= TrankAdapter(activity,R.layout.layout_trank_item,data)
        fview!!.lv_trank_list.adapter=adapter
        fview!!.srl_trank_fresh.setOnRefreshListener {
            getData(0)
        }
        fview!!.srl_trank_fresh.setPrimaryColorsId(R.color.trank_title, android.R.color.white)
        val date=Date()
        val simpleDateFormat=SimpleDateFormat("yyyyMMdd")
        day= simpleDateFormat.format(date)
        val sh=SimpleDateFormat("HH")
        time=Integer.valueOf(sh.format(date))
        hour=time
        //初始化获取数据
        getData(0)
        initEvent()
        fview!!.linear_last.setOnClickListener {
            if(hour>0){
                getData(-1)
            }
        }

        fview!!.linear_next.setOnClickListener {
            if(hour<time){
                getData(1)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getData(step:Int){
        hour+=step
        GetServiceData.getData(activity, "$url?date=$day&hour=$hour",handler,0,Result::class.java)
        setTvIm()
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun setTvIm(){
        if(hour in 1..(time - 1)){
            fview!!.tv_trank_last.setTextColor(resources.getColor(R.color.trank_last_y,null))
            fview!!.iv_trank_last.setImageResource(R.mipmap.last)
            fview!!.tv_trank_next.setTextColor(resources.getColor(R.color.trank_last_y,null))
            fview!!.iv_trank_next.setImageResource(R.mipmap.next)
        }else {
            if (hour == 0) {
                fview!!.tv_trank_last.setTextColor(resources.getColor(R.color.trank_last_n, null))
                fview!!.iv_trank_last.setImageResource(R.mipmap.last_e6)
            }
            if (hour == time) {
                fview!!.tv_trank_next.setTextColor(resources.getColor(R.color.trank_last_n, null))
                fview!!.iv_trank_next.setImageResource(R.mipmap.next_e6)
            }
        }
        fview!!.tv_h_title.text="小时风云榜 - 今日${hour}时榜"
    }

    @SuppressLint("SetTextI18n")
    private fun initEvent(){
        fview!!.lv_trank_list.setOnItemClickListener { _, _, i, _ ->
            val item=data[i]
            startActivity<InfoDetailActivity>( "item" to item)
        }
    }


}