package com.admin.haitao.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.View
import android.webkit.*
import com.admin.haitao.R
import kotlinx.android.synthetic.main.fragment_nine.view.*

/**
 * Created by 吴昶 on 2018/8/1.
 */
class NineFragment:BaseFragment(){

    var url="https://guangdiu.com/cheaps.php"

    override fun bindLayout(): Int {
        return R.layout.fragment_nine
    }

    @SuppressLint("JavascriptInterface", "SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun initData() {
        fview!!.line_title.setBackgroundColor(resources.getColor(R.color.index,null))
        fview!!.wb_nine.loadUrl(url)
        val webSettings = fview!!.wb_nine.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.setSupportZoom(true)
        webSettings.builtInZoomControls = true
        webSettings.cacheMode = WebSettings.LOAD_NO_CACHE
        fview!!.wb_nine.addJavascriptInterface(JavascriptInterface(activity), "jsObj")
        fview!!.wb_nine.webViewClient= MyWebViewClient(activity)
        fview!!.wb_nine.webChromeClient= MyWebChromeClient()

        //点击webview返回
        fview!!.tv_web_back.setOnClickListener {
            if(fview!!.wb_nine.canGoBack()){
                fview!!.wb_nine.goBack()
            }
            if(!fview!!.wb_nine.canGoBack()){
                fview!!.tv_web_back.visibility=View.INVISIBLE
            }
        }
    }


    inner class JavascriptInterface(val context: Context) {

        fun openImage(img: String) {
            Log.d("00000",img)
        }

    }

    inner class MyWebChromeClient: WebChromeClient(){
        override fun onReceivedTitle(view: WebView?, title: String?) {
            super.onReceivedTitle(view, title)
            Log.d("title",title)
        }
    }

    inner class MyWebViewClient(var context: Context): WebViewClient(){
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            val url=request!!.url.toString()
            Log.d("should",url)
            if(fview!!.wb_nine.canGoBack()){
                fview!!.tv_web_back.visibility=View.VISIBLE
            }
            return super.shouldOverrideUrlLoading(view, request)
        }
    }


}