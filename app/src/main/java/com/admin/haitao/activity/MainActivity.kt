package com.admin.haitao.activity

import android.annotation.SuppressLint
import android.app.Fragment
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.design.internal.BottomNavigationItemView
import android.support.design.widget.BottomNavigationView
import android.view.KeyEvent
import android.view.MenuItem
import com.admin.haitao.R
import com.admin.haitao.base.BaseActivity
import com.admin.haitao.fragment.HomepagerFragment
import com.admin.haitao.fragment.HtpagerFragment
import com.admin.haitao.fragment.HtrankFragment
import com.admin.haitao.fragment.NineFragment
import com.admin.haitao.until.BottomNavigationViewHelper
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by 吴昶 on 2018/7/19.
 */
class MainActivity: BaseActivity(),BottomNavigationView.OnNavigationItemSelectedListener {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.main_home->{
                        setFragment(homepagerFragment)
                        setStatusColor(R.color.index)
                    }
                    R.id.main_ht->{
                        setFragment(htpagerFragment)
                        setStatusColor(R.color.ht_title)
                    }
                    R.id.main_hbank->{
                        setFragment(htrankFragment)
                        setStatusColor(R.color.trank_title)
                    }
                    R.id.main_nine->{
                        setFragment(nineFragment)
                        setStatusColor(R.color.index)
                    }
        }
        return true
    }

    private var homepagerFragment=HomepagerFragment()
    private var htpagerFragment= HtpagerFragment()
    private var htrankFragment=HtrankFragment()
    private var nineFragment=NineFragment()
    override fun setLayoutId(): Int {
        return R.layout.activity_main
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun initData() {
        bnv_v.setOnNavigationItemSelectedListener(this)
        bnv_v.selectedItemId=R.id.main_home
        BottomNavigationViewHelper.disableShiftMode(bnv_v)
    }

    private fun setFragment(fragment: Fragment){
        val transaction=fragmentManager.beginTransaction()
        transaction.replace(R.id.fl_body,fragment)
        transaction.commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            val home = Intent(Intent.ACTION_MAIN)
            home.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            home.addCategory(Intent.CATEGORY_HOME)
            startActivity(home)
            return false
        }
        return super.onKeyDown(keyCode, event)
    }
}