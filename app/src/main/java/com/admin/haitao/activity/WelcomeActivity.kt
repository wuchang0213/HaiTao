package com.admin.haitao.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.admin.haitao.R
import kotlinx.android.synthetic.main.activity_welcom.*
import org.jetbrains.anko.startActivity

class WelcomeActivity : AppCompatActivity() {

    private var handler= Handler()
    private var runable= Runnable {
        kotlin.run {
            startActivity<MainActivity>()
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcom)

        val explain=resources.getString(R.string.explain)
        val text=explain.replace(",","\n\n")
        tv_explain.text=text
        handler.postDelayed(runable,2000)
    }



}
