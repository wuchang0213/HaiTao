package com.admin.haitao.activity

import android.app.Application
import com.admin.haitao.until.SharedPreferencesUtil
import com.admin.haitao.until.VolleyUtil

/**
 * Created by 吴昶 on 2018/7/26.
 */
open class MyApplication:Application(){
    

    override fun onCreate() {
        super.onCreate()
        VolleyUtil.initialize(applicationContext)
        SharedPreferencesUtil.getInitial(applicationContext)
    }
}