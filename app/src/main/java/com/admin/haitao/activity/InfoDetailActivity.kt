package com.admin.haitao.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import com.admin.haitao.R
import com.admin.haitao.base.BaseActivity
import com.admin.haitao.table.InfoDetail
import kotlinx.android.synthetic.main.activity_info_detail.*
import kotlinx.android.synthetic.main.layout_title.*
import android.view.KeyEvent
import android.webkit.*
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import android.content.pm.PackageManager
import android.content.Intent


/**
 * Created by 吴昶 on 2018/7/27.
 */
class InfoDetailActivity:BaseActivity(){

    private val url="https://guangdiu.com/api/showdetail.php"

    override fun setLayoutId(): Int {
        return R.layout.activity_info_detail
    }

    @SuppressLint("JavascriptInterface", "SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun initData() {
        setStatusColor(R.color.index)
        line_bar.setBackgroundColor(resources.getColor(R.color.index,null))
        iv_title_src.setImageResource(R.mipmap.back)
        val infodetail=intent.getSerializableExtra("item") as InfoDetail
        Log.d("=",infodetail.toString())
        val conn="$url?id=${infodetail.id}&btn=1&kepler=1&bc=1"
        Log.d("=",conn)
        tv_h_title.text=infodetail.title

        wb_detail.loadUrl(conn)
        val webSettings = wb_detail.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.setSupportZoom(true)
        webSettings.builtInZoomControls = true
        webSettings.cacheMode = WebSettings.LOAD_NO_CACHE
        wb_detail.addJavascriptInterface(JavascriptInterface(), "jsObj")
        wb_detail.webViewClient= MyWebViewClient(this)
        wb_detail.webChromeClient= MyWebChromeClient()

        iv_title_src.setOnClickListener {
            finish()
        }
    }


    inner class JavascriptInterface {
        fun openImage(jsObj: String) {

        }
    }

    inner class MyWebChromeClient:WebChromeClient(){
        override fun onReceivedTitle(view: WebView?, title: String?) {
            super.onReceivedTitle(view, title)
            tv_h_title.text=title
        }
    }

    inner class MyWebViewClient(var context: Context):WebViewClient(){
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            val url=request!!.url.toString()
            Log.d("should",url)
            if(url.indexOf("tbopen://m.taobao.com")!=-1) {
                return if (isAppInstalled(context, "com.taobao.taobao")) {
                    val intent2 = Intent()
                    intent2.action = "android.intent.action.VIEW"
                    intent2.data = request.url
                    startActivity(intent2)
                    false
                }else{
                    super.shouldOverrideUrlLoading(view, request)
                }
            }
            return super.shouldOverrideUrlLoading(view, request)
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        Log.i("ansen", "是否有上一个页面:" + wb_detail.canGoBack())
        if (wb_detail.canGoBack() && keyCode == KeyEvent.KEYCODE_BACK) {//点击返回按钮的时候判断有没有上一页
            wb_detail.goBack() // goBack()表示返回webView的上一页面
            return true
        }
        return super.onKeyDown(keyCode, event)
    }


    private fun isAppInstalled(context: Context, uri: String): Boolean {
        val pm = context.packageManager
        return try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }



}
