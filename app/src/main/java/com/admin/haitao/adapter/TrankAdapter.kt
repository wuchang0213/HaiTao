package com.admin.haitao.adapter

import android.annotation.SuppressLint
import android.content.Context
import com.admin.haitao.R
import com.admin.haitao.table.InfoDetail
import com.admin.haitao.until.ImageUtils
import com.admin.kotlintest.base.AdapterHolder
import com.admin.kotlintest.base.CurrencyAdapter
import java.text.SimpleDateFormat

/**
 * Created by 吴昶 on 2018/7/26.
 */
class TrankAdapter(context: Context, layout: Int, data:MutableList<InfoDetail>) :CurrencyAdapter<InfoDetail>(context,layout,data){

    @SuppressLint("SetTextI18n")
    override fun convert(holder: AdapterHolder, item: InfoDetail, position: Int) {
        holder.getTextView(R.id.tv_trank_number)!!.text="No.$position"
        val image=holder.getImageView(R.id.iv_trank_header)
        ImageUtils.GlideLoadImage(context,item.image,image,R.mipmap.loading_error,R.mipmap.loading_ing)
        holder.getTextView(R.id.tv_trank_title)!!.text=item.title
        holder.getTextView(R.id.tv_trank_mall)!!.text=item.mall

        val format=SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date=format.parse(item.pubtime).time
        val n=System.currentTimeMillis()
        var t=((n-date)/1000/60)//分钟
        val from=if (item.fromsite==""){ "" }else{ "·${item.fromsite}" }
        val time: String
        if(t>60){
            t /= 60
            time = if(t>24){
                t/=24
                "${t}天前$from"
            }else{
                "${t}小时前$from"
            }
        }else{
            time="${t}分钟前$from"
        }
        holder.getTextView(R.id.tv_trank_time)!!.text=time
    }

}