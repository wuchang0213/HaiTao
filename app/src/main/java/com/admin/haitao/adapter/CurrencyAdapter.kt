package com.admin.kotlintest.base

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

/**
 * Created by admin on 2017/11/28.
 * listview grideview 的通用适配器
 */

abstract class CurrencyAdapter<T>(context: Context, layoutId: Int, data: MutableList<T>?) : BaseAdapter() {

    protected var context: Context?=null
    var layoutId:Int=0
    var data:MutableList<T> ?=null

    init {
        this.context=context
        this.layoutId=layoutId
        this.data=data
    }


    fun removeItem(position: Int) {
        data!!.removeAt(position)
        notifyDataSetChanged()
    }

    /**
     * 提供数据更新接口
     * @param data
     */
    fun upData(data: MutableList<T>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return data?.size ?:0
    }

    override fun getItem(i: Int): T  {
        return data!![i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View ?{
        var contentView=view
        val holder: AdapterHolder
        if (contentView == null) {//listview 的view 复用的属性
            holder = AdapterHolder.getAdapterHolder(context!!, contentView, viewGroup, layoutId, i)
            contentView = holder.getContentView()// 此处需要将view 赋值,如果不赋值此view复用的时候就为空,列表会混乱
        } else {
            holder = contentView.tag as AdapterHolder
        }
        convert(holder, getItem(i), i)
        //如果此处直接返回 holder.getContentView() 可能会导致在滑动的过程中出现空白,数据消失等问题
        return contentView
    }

    abstract fun convert(holder: AdapterHolder, item: T, position: Int)

}
