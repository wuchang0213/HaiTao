package com.admin.haitao.adapter

import android.content.Context
import android.view.View
import com.admin.haitao.R
import com.admin.haitao.table.Type
import com.admin.kotlintest.base.AdapterHolder
import com.admin.kotlintest.base.CurrencyAdapter

/**
 * Created by 吴昶 on 2018/7/27.
 */
class TypeAdapter(context: Context,layout:Int,data:MutableList<Type>):CurrencyAdapter<Type>(context,layout,data){

    var choseId=1

    override fun convert(holder: AdapterHolder, item: Type, position: Int) {
        val typeName=holder.getTextView(R.id.tv_type_name)!!
        val typeLine=holder.getTextView(R.id.tv_type_line)!!
        if(item.type=="=="){
            typeName.visibility=View.GONE
            typeLine.visibility=View.VISIBLE
        }else{
            typeName.visibility=View.VISIBLE
            typeName.text=item.title
            typeLine.visibility=View.GONE
            typeName.isSelected = position==choseId
        }
    }


}