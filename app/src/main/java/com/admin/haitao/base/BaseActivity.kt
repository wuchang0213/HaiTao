package com.admin.haitao.base

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

/**
 * Created by 吴昶 on 2018/7/19.
 */
abstract class BaseActivity : AppCompatActivity(){

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(setLayoutId())
        initData()
    }

    abstract fun setLayoutId():Int

    abstract fun initData()

    open fun toast(context: Context, message:String){
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    open fun setStatusColor(color:Int){
        window.statusBarColor = resources.getColor(color,null)
    }
}