# HaiTao

#### 项目介绍
kotlin学习项目，仿照逛丢，数据来源逛丢，仅供kotlin学习使用不得作为任何商业用途

#### 软件架构
使用volley网络请求框架，使用gson解析json数据。整个项目使用kotlin开发


#### 安装教程
直接克隆或者下载后导入Android studio运行即可

#### 使用说明
1、Android studio 导入后直接运行或者打包安装即可
2、本项目仅作为学习使用
3、本项目不得作为任何商业用途

#### 参与贡献
1、界面以及数据来源均参考逛丢APP（逛丢官网 https://guangdiu.com）

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)